#!/bin/sh
FLATPAK_RIMWORLD_FOLDER="$HOME/.var/app/com.valvesoftware.Steam/.local/share/Steam/steamapps/common/RimWorld/Mods/"
RIMWORLD_FOLDER="$HOME/.local/share/Steam/steamapps/common/RimWorld/Mods/"
MOD_TO_INSTALL="ucl gauche"

if [ -d "$FLATPAK_RIMWORLD_FOLDER" ]; then
  echo "Flatpak Steam version"
   cp -r $MOD_TO_INSTALL $FLATPAK_RIMWORLD_FOLDER
fi
if [ -d "$RIMWORLD_FOLDER" ]; then
  echo "Package Steam version"
  cp -r $MOD_TO_INSTALL $RIMWORLD_FOLDER
fi

